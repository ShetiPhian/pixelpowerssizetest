package shetiphian.sizetester.common;

import cpw.mods.fml.common.FMLCommonHandler;

public class ProxyCommon
{
	public void registerEventHandlers()
	{
		FMLCommonHandler.instance().bus().register(new SizeHandlerServer());
	}
}