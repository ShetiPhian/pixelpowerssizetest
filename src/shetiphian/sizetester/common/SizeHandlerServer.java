package shetiphian.sizetester.common;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;

public class SizeHandlerServer
{
	@SubscribeEvent
	public void playerTick(TickEvent.PlayerTickEvent event)
	{
		SizeFunctions.playerSize.put("Player343", (byte)3);
		SizeFunctions.playerSize.put("Player532", (byte)1);

		if(event.phase != TickEvent.Phase.END || !event.side.isServer()) {
			return;
		}

		byte playersize = SizeFunctions.getPlayerSize(event.player); 
		double ySize = event.player.yOffset - SizeFunctions.sizeValues[playersize][2];
		event.player.lastTickPosY += ySize;
		event.player.prevPosY += ySize;
		event.player.posY += ySize;

		if (event.player.height != SizeFunctions.sizeValues[playersize][1]) {
			SizeFunctions.setSize(event.player, playersize);
		}
	}
}