package shetiphian.sizetester.common;

import java.util.HashMap;
import java.util.Map;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;

public class SizeFunctions
{
	// PlayerWidth, PlayerHeight, EyeHeight
	public static final float[][] sizeValues = { {0.4F, 0.8F, 0.74F}, {0.6F, 1.3F, 1.2F}, {0.6F, 1.8F, 1.67F}, {0.8F, 2.3F, 2.0F} };

	public static Map<String, Byte> playerSize = new HashMap<String, Byte>();

	public static byte getPlayerSize(EntityPlayer player)
	{
		if (SizeFunctions.playerSize.containsKey(player.getDisplayName())) {
			return SizeFunctions.playerSize.get(player.getDisplayName());
		}
		return 2;
	}

	public static void setSize(EntityPlayer player, int size)
	{
		size = MathHelper.clamp_int(size, 0, 3);
		float width = SizeFunctions.sizeValues[size][0];
		float height = SizeFunctions.sizeValues[size][1];
		if (width != player.width || height != player.height) {
			player.width = width;
			player.height = height;
			player.boundingBox.maxX = player.boundingBox.minX + (double)player.width;
			player.boundingBox.maxZ = player.boundingBox.minZ + (double)player.width;
			player.boundingBox.maxY = player.boundingBox.minY + (double)player.height;
		}
		float f2 = player.width % 2.0F;
		if (f2 < 0.375D) {
			player.myEntitySize = Entity.EnumEntitySize.SIZE_1;
		} else if (f2 < 0.75D) {
			player.myEntitySize = Entity.EnumEntitySize.SIZE_2;
		} else if (f2 < 1.0D) {
			player.myEntitySize = Entity.EnumEntitySize.SIZE_3;
		} else if (f2 < 1.375D) {
			player.myEntitySize = Entity.EnumEntitySize.SIZE_4;
		} else if (f2 < 1.75D) {
			player.myEntitySize = Entity.EnumEntitySize.SIZE_5;
		} else {
			player.myEntitySize = Entity.EnumEntitySize.SIZE_6;
		}

		player.setPosition(player.posX, player.posY, player.posZ);
		player.eyeHeight = height - player.yOffset;
	}
}