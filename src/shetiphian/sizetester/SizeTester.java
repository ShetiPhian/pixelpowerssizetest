package shetiphian.sizetester;
import shetiphian.sizetester.common.ProxyCommon;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;

@Mod(
		modid = "SizeTester",
		name = "SizeTester",
		version = "ALPHA1")
public class SizeTester
{
	@SidedProxy(
			clientSide = "shetiphian.sizetester.client.ProxyClient",
			serverSide = "shetiphian.sizetester.common.ProxyCommon")
	public static ProxyCommon		proxy;

	@Instance("SizeTester")
	public static SizeTester	INSTANCE;

	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		proxy.registerEventHandlers();
	}
}

/* TODO: Issue List
 * - Rendering is incorrect in multiplayer
 * - Player takes damage when jumping into a block
 * - Bed rendering wrong
 * - Mounted offset is wrong
 * - In player inventory: smallest players too small, largest player too large
 * - Small player has large shadow, others have none
 */
