package shetiphian.sizetester.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraftforge.client.event.RenderPlayerEvent;
import org.lwjgl.opengl.GL11;
import shetiphian.sizetester.common.SizeFunctions;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;

public class SizeHandlerClient
{
	private Minecraft	mc	= FMLClientHandler.instance().getClient();

	private float defaultHeight = 1.8F;
	private float ySize;
	private float eyeHeight;

	@SubscribeEvent
	public void renderTick(TickEvent.RenderTickEvent event)
	{
		if(mc.theWorld == null) {
			return;
		}

		byte playersize = SizeFunctions.getPlayerSize(mc.thePlayer); 
		if (playersize != 2) {
			if(event.phase == TickEvent.Phase.START) {
				ySize = mc.thePlayer.yOffset - SizeFunctions.sizeValues[playersize][2];
				eyeHeight = mc.thePlayer.eyeHeight;
				mc.thePlayer.lastTickPosY -= ySize;
				mc.thePlayer.prevPosY -= ySize;
				mc.thePlayer.posY -= ySize;
				mc.thePlayer.eyeHeight = (mc.thePlayer.height / defaultHeight < 1.0F) ? (mc.thePlayer.height / defaultHeight * mc.thePlayer.getDefaultEyeHeight()) : mc.thePlayer.getDefaultEyeHeight();
			} else {
				mc.thePlayer.lastTickPosY += ySize;
				mc.thePlayer.prevPosY += ySize;
				mc.thePlayer.posY += ySize;
				mc.thePlayer.eyeHeight = eyeHeight;
			}
		}
	}

	@SubscribeEvent
	public void worldTick(TickEvent.ClientTickEvent event)
	{
		if(event.phase != TickEvent.Phase.END || Minecraft.getMinecraft().theWorld == null) {
			return;
		}
		byte playersize = SizeFunctions.getPlayerSize(mc.thePlayer); 
		if (mc.thePlayer.height != SizeFunctions.sizeValues[playersize][1]) {
			SizeFunctions.setSize(mc.thePlayer, playersize);
		}
	}

	@SubscribeEvent
	public void onRenderPlayer(RenderPlayerEvent.Pre event)
	{
		byte playersize = SizeFunctions.getPlayerSize(event.entityPlayer);
		if (playersize == 0) { // Corrects lighting on small players
			int brightness = playerBrightness(event.entityPlayer);
			int j = brightness % 65536;
			int k = brightness / 65536;
			OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, (float)j / 1.0F, (float)k / 1.0F);
		}

		if (playersize != 2) {
			GL11.glPushMatrix();
			if (playersize == 0) {
				GL11.glScalef(0.44F, 0.44F, 0.44F);
			} else if (playersize == 1) {
				GL11.glScalef(0.8F, 0.74F, 0.8F);
			} else if (playersize == 3) {
				GL11.glScalef(1.2F, 1.24F, 1.2F);
			}
		}
	}

	public int playerBrightness(EntityPlayer player)
	{
		if (player.isBurning()) {
			return 15728880;
		} else {
			int x = MathHelper.floor_double(player.posX);
			int z = MathHelper.floor_double(player.posZ);
			if (player.worldObj.blockExists(x, 0, z)) {
				double d0 = (player.boundingBox.maxY - player.boundingBox.minY) * 0.66D;
				int y = MathHelper.floor_double(player.posY - (double)player.yOffset + d0);
				return player.worldObj.getLightBrightnessForSkyBlocks(x, y + 1, z, 0);
			} else {
				return 0;
			}
		}
	}

	@SubscribeEvent
	public void onRenderPlayer(RenderPlayerEvent.Post event)
	{
		byte playersize = SizeFunctions.getPlayerSize(event.entityPlayer);
		if (playersize != 2) {
			GL11.glPopMatrix();
		}
	}
}