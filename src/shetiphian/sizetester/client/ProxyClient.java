package shetiphian.sizetester.client;

import shetiphian.sizetester.common.ProxyCommon;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.common.FMLCommonHandler;

public class ProxyClient extends ProxyCommon
{
	@Override
	public void registerEventHandlers()
	{
		super.registerEventHandlers();
		SizeHandlerClient sizeHandler = new SizeHandlerClient();
		FMLCommonHandler.instance().bus().register(sizeHandler);
		MinecraftForge.EVENT_BUS.register(sizeHandler);
	}
}